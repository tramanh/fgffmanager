package com.forgetfilefast;

import java.io.*;
import java.net.*;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.CountDownLatch;


public class DownloadMission {
	public static final int READY = 1;
	public static final int DOWNLOADING = 2;
	public static final int ERROR = 3;
	public static final int FINISHED = 4;
	
	public static final int DEFAULT_THREAD_COUNT = 32;
	public static final String DEFAULT_DIRECTORY = "./root/";
	
	private String IS_url;
	private String save_filename;
	private String real_savename;
	private String hash;
	private String save_directory;
	private long content_size;
	private int status;
	private int thread_count;
	private byte[] storage;
	private CountDownLatch latch;
	private JProgressBar progressBar;
	private JList list;
	private JLabel label;
	
	private Long time;
	
	
	private ArrayList <String> MS_url;
	
	public DownloadMission(String IS_url, String namehash, String save_directory, int thread_count, JProgressBar progressBar, JList list, JLabel label, String save_filename) {
		this.IS_url = IS_url;
		String[] tmp = namehash.split("/");
		this.real_savename = save_filename;
		this.save_filename = tmp[0];
		this.hash = tmp[1];
		this.status = READY;
		this.save_directory = save_directory;
		this.thread_count = thread_count;
		this.MS_url = new ArrayList<String>();
		this.progressBar = progressBar;
		this.list = list;
		this.label = label;
		if(this.label!=null)label.setText("READY");
		init();
	}
	
	public DownloadMission(String IS_url, String namehash, String save_directory, int thread_count, JProgressBar progressBar, JList list) {
		this.IS_url = IS_url;
		String[] tmp = namehash.split("/");
		this.real_savename = save_directory + "/" + tmp[0];
		this.save_filename = tmp[0];
		this.hash = tmp[1];
		this.save_directory = save_directory;
		this.status = READY;
		this.thread_count = thread_count;
		this.MS_url = new ArrayList<String>();
		this.progressBar = progressBar;
		this.list = list;
		init();
	}
	
	
	public DownloadMission(String IS_url, String filename, String save_directory, JProgressBar progressBar, JList list) {
		this(IS_url, filename, save_directory, DEFAULT_THREAD_COUNT, progressBar, list);
	}

	public DownloadMission(String IS_url, String filename, String save_directory, JProgressBar progressBar) {
		this(IS_url, filename, save_directory, DEFAULT_THREAD_COUNT, progressBar, null);
	}

	public DownloadMission(String IS_url, String filename, int thread_count, JProgressBar progressBar, JList list) {
		this(IS_url, filename, DEFAULT_DIRECTORY, thread_count, progressBar, list);
	}	

	public DownloadMission(String IS_url, String filename, int thread_count, JProgressBar progressBar) {
		this(IS_url, filename, DEFAULT_DIRECTORY, thread_count, progressBar, null);
	}	

	public DownloadMission(String IS_url, String filename, JProgressBar progressBar, JList list) {
		this(IS_url, filename, DEFAULT_DIRECTORY, DEFAULT_THREAD_COUNT, progressBar, list);
	}
	public DownloadMission(String IS_url, String filename, JProgressBar progressBar) {
		this(IS_url, filename, DEFAULT_DIRECTORY, DEFAULT_THREAD_COUNT, progressBar, null);
	}
	
	
	public void init() {
		try {
			time = Instant.now().toEpochMilli();
			String parameter = String.format("/get?f=%s", this.save_filename + "/" + this.hash);
			URL obj = new URL(this.IS_url + parameter);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			
			int responseCode = con.getResponseCode();
			if (responseCode != 200) {
				System.out.println("Internal Error");
				this.status = ERROR;
				if(label!=null)label.setText("ERROR");
				return;
			}

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.isEmpty())
					break;
				inputLine += String.format("dl?filename=%s", this.save_filename);
				this.MS_url.add(inputLine);
			}
			in.close();
			this.content_size = getContentLength();	
			if (this.content_size < 2*thread_count )
				this.thread_count = 1;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
	}
	
	private long getContentLength() {
		int  idx = 0;
		long res = -1;
		
			for (; idx < this.MS_url.size(); idx++) {
				try {
					URL obj = new URL(this.MS_url.get(idx));
					HttpURLConnection con = (HttpURLConnection) obj.openConnection();
					con.setRequestMethod("GET");
					String range_str = String.format("bytes=0-1");
					con.setRequestProperty("Range", range_str);
					
					int responseCode = con.getResponseCode();
					if (res == -1) {
						try{
							String raw = con.getHeaderField("Content-range");
							res =  Long.valueOf(raw.split("/")[1]);
						}
						catch (NullPointerException e) {
							res = 0L;
						}
					}
				} catch (Exception ex) {
//					System.out.println(this.MS_url.get(idx));
					ex.printStackTrace();
					((DefaultListModel) this.list.getModel()).addElement("Can't connect to " +
							this.MS_url.get(idx));
					this.MS_url.remove(idx);
				}
			}
		
		if (this.MS_url.size() == 0)
			((DefaultListModel) this.list.getModel()).addElement("No active host found");
		if(this.progressBar != null)
			this.progressBar.setMaximum(MS_url.size());
		return res;
	}
	
	public void start() {
		if (this.content_size == -1) {
			if(label!=null)label.setText("ERROR");
			this.status = ERROR;
			return;
		}
		if(label!=null)label.setText("DOWNLOADING");
		latch = new CountDownLatch(this.thread_count);
		long end = -1, start, range_size = this.content_size / this.thread_count + 1;
//		if (this.content_size == 0) range_size = 0; 
		for (int i = 0; i < this.thread_count; i++) {
			start = end + 1;
			end = start + range_size;
			if (end >= this.content_size)
				end = this.content_size - 1;
			String url = this.MS_url.get(i % this.MS_url.size());
			Thread dr = new DownloadRunnable(url, this.real_savename, start, end, latch, this.progressBar, this.list);
			dr.start();
		}
		this.status = DOWNLOADING;
		
		wait2finish();
	}
	
	private void wait2finish() {
		try {
			this.latch.await();
			finish();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void finish() {
		try {
//			FileOutputStream output_stream = new FileOutputStream(this.real_savename);
//			output_stream.write(this.storage);
//			output_stream.close();
			this.status = FINISHED;
			if(label!=null)label.setText("FINISHED");
			((DefaultListModel) this.list.getModel()).addElement("Total download time:" + String.valueOf(Instant.now().toEpochMilli() - time) + "ms");
			if (!getSHA256(real_savename).equals(hash))
				((DefaultListModel) this.list.getModel()).addElement("!!!!!!!!!Invalid checksum!!!!!!!!");
			else
				((DefaultListModel) this.list.getModel()).addElement("Valid checksum!");
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private String getSHA256(String file) {
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			
			FileInputStream fis = new FileInputStream(file);
	        byte[] dataBytes = new byte[1024];
	     
	        int nread = 0; 
	        while ((nread = fis.read(dataBytes)) != -1) {
	          md.update(dataBytes, 0, nread);
	        }
	        return bytesToHex(md.digest());
		}
		catch (Exception ex)
		{
			((DefaultListModel) this.list.getModel()).addElement("Exception"
					+ ex.toString());
		}
		
		return bytesToHex(new byte[32]);
	}
	private static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
	}
	public static void main(String[] args) {
		JFrame frame = new JFrame();
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 
	    JProgressBar aJProgressBar = new JProgressBar(0, 50);
	    aJProgressBar.setStringPainted(true);

	    JButton aJButton = new JButton("Start");
	    ActionListener actionListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	          aJButton.setEnabled(false);
	          aJProgressBar.setMaximum(6);
	          DownloadMission m = new DownloadMission("http://127.0.0.1:8080", "123/dummy", aJProgressBar);
	  		  m.start();
	        }
	    };
	    aJButton.addActionListener(actionListener);
	    frame.add(aJProgressBar, BorderLayout.NORTH);
	    frame.add(aJButton, BorderLayout.SOUTH);
	    frame.setSize(300, 200);
	    frame.setVisible(true);
	}
}
